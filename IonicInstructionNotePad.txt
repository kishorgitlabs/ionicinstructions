


IONIC WEB SQL :
https://developmobileapplications.com/ionic-v3-angular-5-websql-advance


DEVICE DEBUG :

chrome://inspect/devices#devices


CSS ClipPath :

https://bennettfeely.com/clippy/
 
CK EDITOR :

https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/frameworks/angular.html



Add Git in Laptop

Search windows credentials manager and edit or remove and insert our git lab username and password .

https://bennettfeely.com/clippy/  ====> Clip Path
https://cssgradient.io/           ====>Gradinet


ionic start demo blank --type=angular --cordova


Ionic Color :

https://ionicframework.com/docs/theming/colors

Git Download

  1.   git init
  
  2 .  git add .

  3 .   git commit -m "Intial commit"

  4.   git url

  5 .  git push -u origin master



Genarate Random Number :

var min = 10000;
var max = 90000;
 this.randomNum = Math.floor(Math.random() * min) + max;


Ionic Root Detection :

https://stackoverflow.com/questions/48656741/how-to-restrict-ionic-2-application-installation-if-device-is-rooted-or-jail-bro


Random Captch Generate :

makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 7; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

   this.getText=text;

  }
  



IONIC TIME OUT FUNCTIONALITY : 

https://developmobileapplications.com/how-to-implement-idle-timeout-in-ionic-5-idle-timeout-example-for-ionic-5

CODING :

idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
 
  constructor(private idle: Idle, private keepalive: Keepalive) {

    // sets an idle timeout of 10 seconds.
    idle.setIdle(60);  //After Starting time

    // sets a timeout period of 10 seconds. after 20 seconds of inactivity, the user will timed out.
    idle.setTimeout(7);

    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

idle.onIdleEnd.subscribe(() => alert("Idle End"));    

idle.onTimeout.subscribe(() => {       
       alert("Time out");   
});    
idle.onIdleStart.subscribe(() => alert("Idle Start"));   
 
idle.onTimeoutWarning.subscribe((countdown) => 
alert("You have only "+countdown+'Seconds'));     // sets the ping interval to 15 seconds    

keepalive.interval(15);     
 

this.idle.watch();

Ionic Color Adding in Variable :

https://ionicframework.com/docs/theming/colors

DYNAMIC FORMS :

HTML :
<div formArrayName="users">
<label>Name</label><br>
<div *ngFor ="let control of loginForm.controls.users['controls'];let i=index;" [formGroupName]="i">
<input type="text" formControlName="names" >
<input type="text" formControlName="ages">
<button (click)="remove(i)">REMOVE</button>
</div>
</div>

<div>
    <button (click)="addUser()">ADD NEW USER</button>
</div>


TS:

ngOnInit() {
  
this.loginForm=this.fb.group({  
  'users':new FormArray([
    this.fb.group({
      'names':new FormControl(''),
      'ages':new FormControl('')
    })
  ])
});
  }
  remove(i:any){
   let user=this.loginForm.get('users') as FormArray;
   user.removeAt(i);
  }
  getUsers():FormArray{
    return this.loginForm.get('users') as FormArray;
  }

  addUser(){
    let user=this.loginForm.get('users') as FormArray;
    let newuser=this.fb.group({
      'names':'',
      'ages':''
    });
    user.push(newuser);
  }



Reactive Forms  : 

http://zerocodeform.com/

Installation and Run in Ionic Project


1 . Download and Install Node and Npm   https://nodejs.org/en/

2 . Download and Install Visual code

3 . cmd  --> npm i -g @ionic/cli

4 . cmd  --> npm i -g cordova

5 . ionic start projectname

6. Run -> ionic serve

7 . Android  -> ionic cordova prepare android  --prod

8 . ionic repair --> Repair all nodemodules

9 . Add Android  --> ionic cordova platform add android

10 . check list  -->  ionic cordova plugin ls





Network Config :

<?xml version="1.0" encoding="utf-8"?>
<network-security-config>
    <base-config cleartextTrafficPermitted="true">
        <trust-anchors>
            <certificates src="system" />
        </trust-anchors>
    </base-config>
    <domain-config cleartextTrafficPermitted="true">
        <domain includeSubdomains="true">brainmagicllc.com</domain>
        <domain includeSubdomains="true">https://brainmagicllc.com</domain>
    </domain-config>
</network-security-config>

Dear Sir/Mam,
 
             I am writing this to inform that, I am resigning from the position of Android Developer from today(03/09/2021). Please accept this letter as formal notice of resignation.
 
             I would like to thank for the opportunity you have given me and for the guidance and support.
 
Regards,
kishor  

}



Image Modifications  

1 . Image Url  --> https://pixlr.com/e/

2 . icon  --> 1024*1024

3 . splash  --> 2732*2732

4 . Paste the icons and splash 

5 . npm i cordova-res 
   ionic cordova resources 

Dynaic Forms :

HTML :
<form [formGroup]="addForms">

  <div class="form-group" formArrayName="users" >
<label>Users</label>
<div *ngFor="let control of addForms.controls.users['controls']; let i=index" [formGroupName]="i">
<input formControlName="name"><button (click)="remove(i)">Remove</button>
<input formControlName="age">
<input formControlName="sex">
</div>
  </div>
  <button (click)="add()">AddUser</button>
</form>

<router-outlet></router-outlet>


Type Script :

  ngOnInit() {

 this.addForms=new FormGroup({

  'users':new FormArray([
    this.fb.group({
      'name':new FormControl(''),
      'age':new FormControl(''),
      'sex':new FormControl('')
    })
  ])
 })
  }

  getUsers():FormArray{
    return this.addForms.get('users') as FormArray
  }

  add(){
    let user=this.addForms.get('users') as FormArray;

    let newUser=this.fb.group({
'name':'',
'age':'',
'sex':''
    })
    user.push(newUser);
  }
  remove(i:any){

    let arr=this.addForms.get('users') as FormArray;
    arr.removeAt(i)
  }


GOOGLE LOGIN IN ANGULAR : 
 Index.html : 
      
    <script src="https://accounts.google.com/gsi/client" async defer></script>
HTML : 
   
          <div id="buttonDiv" (click)="doLogin()"> 
          
          </div> 
TS: 
  doLogin(){
      try{
        // (window as any).onGoogleLibraryLoad = () => {
          
          
          (window as any).google?.accounts.id.initialize({
            client_id: '815254895580-tcrf43ehcfttu8f0jq2c27f7op6etjs5.apps.googleusercontent.com',
            callback: this.handleCredentialResponse.bind(this),
             auto_select: true,
            cancel_on_tap_outside: false
          });
          (window as any).google?.accounts.id.renderButton(  
            document.getElementById('buttonDiv'),
              { theme: 'outline',}
          );
          // google?.accounts.id.prompt(( notification: PromptMomentNotification ) => {});
        
      } catch (error) {
       
        throw error;
      
        
    }
  }
  async handleCredentialResponse(res:any) {
    try {
      const token = res.credential;
      localStorage.setItem('sessionToken',token);
      this.router.navigate(['/logout'],{ replaceUrl: true });
    } catch(error) {
      this.router.navigate(['/login']);      
      throw error;
    }
  }
